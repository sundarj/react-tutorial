import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

function calculateWin (squares) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];

    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
        return [squares[a], [a, b, c]];
      }
    }

    return null;
}

function Square (props) {
    const className = "square" + (props.isWinner ? " square-winner" : "");
    return (
        <button className={className} onClick={props.onClick}>
            {props.value}
        </button>
    );
}

class Board extends React.Component {
    renderSquare (i) {
        return (
            <Square
                isWinner={this.props.winningSquares && this.props.winningSquares.includes(i)}
                key={i}
                value={this.props.squares[i]}
                onClick={() => this.props.onClick(i)}
            />
        );
    }

    render () {
        let rows = [];
        for (let i = 0; i < 3; i++) {
            let boardSquares = [];
            for (let j = 0; j < 3; j++) {
                boardSquares.push(this.renderSquare(3 * i + j));
            }
            rows.push(<div className="board-row" key={i}>{boardSquares}</div>)
        }
        return (
            <div>
                {rows}
            </div>
        );
    }
}

class Game extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            history: [{
                // squares is an array of 9 null, "X", or "O"
                squares: Array(9).fill(null),
                // currentSquare is the square the user last clicked
                currentSquare: null,
            }],
            // stepNumber is the game's turn counter
            stepNumber: 0,
            // X is starting
            xIsNext: true,
            // sort the moves in ascending order by default
            // valid options are "ascending" or "descending"
            sortOrder: "ascending",
        };
        this.handleClick = this.handleClick.bind(this)
        this.handleSortChange = this.handleSortChange.bind(this);
    }

    handleClick (i) {
        // If we "go back in time" and make a new move from that point, we then
        // throw all the past future away
        const history = this.state.history.slice(0, this.state.stepNumber + 1);
        const current = history[history.length - 1];
        // make a copy of current.squares
        const squares = current.squares.slice();
        // if there is a winner or the square is already filled, do nothing
        if (calculateWin(squares) || squares[i]) {
            return;
        }
        squares[i] = this.state.xIsNext ? "X" : "O";
        this.setState({
            history: history.concat([{
                squares: squares,
                currentSquare: i,
            }]),
            stepNumber: history.length,
            xIsNext: !this.state.xIsNext,
        });
    }

    jumpTo (step) {
        this.setState({
            stepNumber: step,
            xIsNext: (step % 2) === 0,
        })
    }

    handleSortChange (event) {
        this.setState({
            sortOrder: event.target.value,
        });
    }

    render () {
        const history = this.state.history;
        const current = history[this.state.stepNumber];
        const win = calculateWin(current.squares);
        // map from index to (column, row)
        const iToColRow = [
            "(1, 1)", "(2, 1)", "(3, 1)",
            "(1, 2)", "(2, 2)", "(3, 2)",
            "(1, 3)", "(2, 3)", "(3, 3)",
        ];
        const moves = history.map((step, move) => {
            // step is the current history item
            // move is the current index
            const currentSquare = step.currentSquare;
            // if move is non-zero show move number and column/row
            // otherwise show the other
            const desc = move ?
                "Go to move #" + move + " " + iToColRow[currentSquare] :
                "Go to game start";
            const isCurrent = move === this.state.stepNumber;
            return (
                <li key={move}>
                    <button onClick={() => this.jumpTo(move)}>
                        {isCurrent ? <strong>{desc}</strong> : desc}
                    </button>
                </li>
            );
        });
        let status;
        let isDraw = current.squares.length === current.squares.filter(x => x !== null).length;
        if (win) {
            status = "Winner: " + win[0];
        } else if (isDraw) {
            status = "Draw"
        } else {
            status = "Next player: " + (this.state.xIsNext ? "X" : "O");
        }

        return (
            <div className="game">
                <div className="game-board">
                    <Board
                        winningSquares={win && win[1]}
                        squares={current.squares}
                        onClick={this.handleClick}
                    />
                </div>
                <div className="game-info">
                    <select value={this.state.sortOrder} onChange={this.handleSortChange}>
                        <option value="ascending">Ascending</option>
                        <option value="descending">Descending</option>
                    </select>
                    <div>{status}</div>
                    <ol>{this.state.sortOrder === "ascending" ? moves : moves.slice().reverse()}</ol>
                </div>
            </div>
        );
    }
}

// ========================================

ReactDOM.render(
    <Game />,
    document.getElementById('root')
);
